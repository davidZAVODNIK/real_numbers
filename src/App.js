import React, { useState } from 'react';
import logo1 from './assets/img/103084.png';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import SelectNumberOfInputs from './components/Selector';
import HorizontalLabelPositionBelowStepper from './components/Stepper';
import { Button, Box, Divider, Typography, TextField } from '@material-ui/core';
import './App.css';

const useStyles = makeStyles(theme => ({
  fab: {
    margin: theme.spacing(1),
  },
  extendedIcon: {
    marginLeft: theme.spacing(1),
  },
}));

function bubblesort(items) {
  const length = items.length;
  for (let i = 0; i < length; i++) {
    for (let j = 0; j < (length - i - 1); j++) {
      if (items[j] > items[j + 1]) {
        [items[j], items[j + 1]] = [items[j + 1], items[j]];
      }
    }
  }
  return items;
};

/**
 * Funkcija, ki sešteje skupno vsoto vseh vnešenih števil, ki so shranjena v tabeli
 */
const arrSum = arr => arr.reduce((a,b) => a + b, 0)

/**
 * Funkcija, ki izračuna povprečje vnešenih števil
 */
const average = data => data.reduce((sum, value) => sum + value) / data.length

/**
 * Funkcija, ki izračuna standardni odklon vnešenih števil
 */
const standardDeviation = values => Math.sqrt(average(values.map(value => (value - average(values)) ** 2)))


function App(props) {
  const classes = useStyles();

  /**
   * Inicializacija vseh potrebnih spremenljivk in njihovih začetnih vrednosti
   */
  const [numberOfInputs, setNumberOfInputs] = useState(0)
  const [inputNumbersProcess, setInputNumbersProcess] = useState(false)
  const [showResult, setShowResult] = useState(false)
  const [data, setData] = useState({
    sum: 0,
    average: 0,
    stDeviation: 0,
    sortArray: []
  })
  const [arrayOfNumbers, setArrayOfNumbers] = useState({
    inputsNumber: []
  }) 
  
  /**
   * Funkcija, ki ponastavi vrednosti in vrne uporabnika na začetno stanje "igre"
   */
  const handleNewGame = () => {
    setShowResult(false)
    setNumberOfInputs(0)
    setArrayOfNumbers({inputsNumber: []})
    setInputNumbersProcess(false)
  }

  const handleChange = (value) => {
    setNumberOfInputs(value === "" ? 0 : value)
  }

  /**
   * Funkcija, ki nastavi vse potrebne rezultate spremenljivkam in nato sproži prikaz končnih rezultatov
   */
  const onShowResult = () => {
    let newTmp = [...arrayOfNumbers.inputsNumber]
    setData({
      sum: arrSum(newTmp),
      average: average(newTmp),
      stDeviation: standardDeviation(newTmp),
      sortArray: bubblesort(newTmp)
    })
    setShowResult(true)
  }

  /**
   * Funkcija, ki služi obladovanji in pomnenju števila, ki ga izberemo na začetku, koliko števi želimo vnesti
   */
  const handleChangeInputsNumber = (stepIndex, event) => {
    let ids = [...arrayOfNumbers.inputsNumber]    
    ids[stepIndex] = event.target.value ? parseInt(event.target.value) : undefined
    setArrayOfNumbers({inputsNumber: ids})
  }
  
  const values = { name: "", email: "", confirmPassword: "", password: "" }

  /**
   * Render funckija, ki v react-u skrbi za renderiranje grafične podobe
   */
  return (
    <div className="App">
      <header className="App-header">
      

        {!inputNumbersProcess && <>
          <img src={logo1} className="App-logo" alt="logo" />
          <Box marginBottom={5}>
            <Divider style={{width: "80%", margin:"auto"}} />
          </Box>
          <Box marginTop={5} marginBottom={5}>
            <Typography variant="h4">Izberite željeno število vnosov realnih števil</Typography>
          </Box>
          {/* SelectNumberOfInputs - komponenta, ki služi izbiri poljubnega števila med 1 in 10 */}
          <SelectNumberOfInputs {...props} onChangeInput={handleChange}/>
          {numberOfInputs > 0 && <Box marginTop={4}>
          <Fab variant="extended" aria-label="next" color="primary" className={classes.fab} onClick={(e) => setInputNumbersProcess(true)}>
            Naprej
            <ArrowForwardIosIcon className={classes.extendedIcon} />
          </Fab>
          </Box>}
        </>}

        
        {/* Prikaz koračnega bloka, kjer je toliko korakov kolikor je bilo izbrano število v prvem koraku */}
        {(inputNumbersProcess > 0 && !showResult) && 
          <Box style={{display: "flex"}}>
            <HorizontalLabelPositionBelowStepper {...props} numberOfInputs={numberOfInputs} inputsNumber={arrayOfNumbers.inputsNumber} handleChangeInputsNumber={handleChangeInputsNumber} onShowResult={onShowResult}/>
          </Box>
        }

        {/* Blok kode, ki prikazuje rezultate */}
        {showResult && 
          <div>
            <Typography variant="h4">Vnešena števila</Typography>
            <ul align="center" style={{display: "inline-block", textAlign: "left"}}>
              {arrayOfNumbers.inputsNumber.map(value => {
                return <li key={value}>{value}</li>
              })}
            </ul>
            <br/>
            <Divider style={{width: "80%", margin:"auto"}} />
            <div style={{display: "inline-block", textAlign: "left"}}>
              <Box margin={3}><Typography variant="h5"><b>Vsota števil:</b> {data.sum}</Typography></Box>
              <Box margin={3}><Typography variant="h5"><b>Sortiranje po metodi bubblesort:</b> {JSON.stringify(data.sortArray)}</Typography></Box>
              <Box margin={3}><Typography variant="h5"><b>Aritmetična sredina:</b> {data.average} </Typography></Box>
              <Box margin={3}><Typography variant="h5"><b>Standardni odklon:</b> {data.stDeviation}</Typography></Box>
            </div>
            <Divider style={{width: "80%", margin:"auto"}} />
            <br/>
            <br/>
            <Button style={{width:"250px"}} variant="contained" color="primary" size="large"  onClick={(e) => {handleNewGame()}}>
              Nov poskus
            </Button>
          </div>
          
      }
      </header>
    </div>
  );
}

export default App;
