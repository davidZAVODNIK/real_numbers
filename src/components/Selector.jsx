import React from "react";
import {
  makeStyles
} from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles(theme => ({
  root: {
    flexWrap: "wrap"
  },
  formControl: {
    margin: theme.spacing(1),
    width: 200
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
}));

export default function SelectNumberOfInputs(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    number: ""
  });

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const handleChange = event => {
    setValues(oldValues => ({
      ...oldValues,
      [event.target.name]: event.target.value
    }));
    props.onChangeInput(event.target.value)
  };

  const renderOptions = () => {
    let options = [];
    for (let i = 1; i <= 10; i++) {
      options.push(<MenuItem key={i} value={i}>{i}</MenuItem>);
    }
    return options;
  };

  return (
    <>
        <form className={classes.root} autoComplete="off">
          <FormControl
            variant="outlined"
            className={classes.formControl}
          >
            <InputLabel
              ref={inputLabel}
              htmlFor="outlined-age-simple"
            >
              Izberi število
            </InputLabel>
            <Select
              value={values.number}
              onChange={handleChange}
              labelWidth={labelWidth}
              variant="outlined"
              inputProps={{
                name: "number",
                id: "outlined-age-simple",
              }}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {renderOptions()}
            </Select>
          </FormControl>
        </form>
    </>
  );
}
