import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { Box } from '@material-ui/core';
import * as Yup from "yup"
import { Formik } from 'formik';

const validationSchema = Yup.object({
  inputNumber: Yup.number()
  .required("Število je obvezno")
})

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  dense: {
    marginTop: theme.spacing(2),
  },
  menu: {
    width: 200,
  },
}));

function getSteps(numberOfInputs) {
    let steps = []
    for(let i=1; i<=numberOfInputs; i++) {
        steps.push(`Število ${i}`)
    }
    return steps;
}

export default function HorizontalLabelPositionBelowStepper(props) {
  console.log('props: ', props);
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps(props.numberOfInputs);
  const [inputError, setInputError] = useState({
    helperText: '', 
    error: false
  })

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleEnd = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
    props.onShowResult()
    handleReset()
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const change = (name, stepIndex, e) => {
    // e.persist();
    if(e.target.value) {
      if (!isNaN(parseInt(e.target.value))) {
        setInputError({ helperText: '', error: false });
        props.handleChangeInputsNumber(stepIndex, e)
      } else {
        setInputError({ helperText: 'Veljavna so samo števila', error: true });
      }
    } else {
      props.handleChangeInputsNumber(stepIndex, e)
    }
  };

  const getStepContent = (stepIndex) => {
    return (
        <>
          <Box marginBottom={4}>
            <Typography variant="h4">Vnesite {stepIndex + 1} število</Typography>
          </Box>
          <TextField
              key={stepIndex}
              required
              id={`inputNumber${stepIndex}`}
              name={`inputNumber${stepIndex}`}
              label="Število"
              helperText={inputError.helperText}
              error={inputError.error}
              value={props.inputsNumber[stepIndex] ? props.inputsNumber[stepIndex] : undefined}
              className={classes.textField}
              onChange={(e) => change(`inputNumber${stepIndex}`, stepIndex, e)}
              // onChange={(e) => props.handleChangeInputsNumber(stepIndex, e)}
              margin="normal"
              variant="outlined"
              type="text"
            />
        </> 
    )
}

  return (
    <div className={classes.root}>
      <Box marginBottom={5}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
      </Box>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography component={'span'} className={classes.instructions}>All steps completed</Typography>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            <form onSubmit={() => {}}>
              <Typography component={'span'} className={classes.instructions}>{getStepContent(activeStep)}</Typography>
              <div>
                <Box marginTop={4}>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.backButton}
                  >
                    Nazaj
                  </Button>
                  <Button variant="contained" color="primary" disabled={inputError.error || (props.inputsNumber[activeStep] === undefined || props.inputsNumber[activeStep] === null)} onClick={e => {activeStep === steps.length -1 ? handleEnd() : handleNext()}}>
                    {activeStep === steps.length - 1 ? 'Zaključi' : 'Naprej'}
                  </Button>
                </Box>
              </div>
            </form>
          </div>
        )}
      </div>
    </div>
  );
}
